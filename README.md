# vilaineaval : Bretagne Vivante et Rennes Vilaine Aval

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Serena et Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier VILAINEAVAL.

Les fichiers produits par R sont dans VILAINEAVAL/images

