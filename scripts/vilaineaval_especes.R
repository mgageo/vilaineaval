# <!-- coding: utf-8 -->
#
# la partie especes
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#
# https://github.com/mtennekes
#
especes <- function() {
#  especes_fiches_protection()
#  especes_lire()
#  especes_ecrire_xlsx()
#  especes_statut()
  especes_combine_stat()
#  especes_combine_atlas()
}
#
# détermination des espèces
especes_faune_lire <- function() {
  spdf <- faune_lire_xls("rva_donnees.xls")
  spdf <- faune_lire_atlas(spdf)
  df <- spdf@data;
#  print(head(df))
  especes.df <- as.data.frame(unique(df[, c("NAME_SPECIES")]))
  colnames(especes.df) <- c("espece")
  especes.df$espece <- as.character(especes.df$espece)
  return(especes.df)
}
especes_fiches_lire <- function() {
  df <- fiches_lire()
  df <- subset(df, Espece!="Pigeon biset domestique")
#  print(head(df))
  especes.df <- as.data.frame(unique(df[, c("Espece")]))
  colnames(especes.df) <- c("espece")
  especes.df$espece <- as.character(especes.df$espece)
  return(especes.df)
}
especes_hiver_lire <- function() {
  df <- atlas_combine_lire()
  especes.df <- as.data.frame(unique(df[, c("espece")]))
  colnames(especes.df) <- c("espece")
  especes.df <- subset(especes.df, espece != "Fuligule hybride milouin x nyroca")
  especes.df$espece <- as.character(especes.df$espece)
  return(especes.df)
}
#
# lecture des différents fichiers espèces
# - hiver : serena
# - nicheurs : excel
# - opportunistes : faune-bretagne
#
especes_lire <- function() {
  if ( exists("especes.df") ) {
#    return(especes.df)
  }
  hiver.df <- especes_hiver_lire()
  fiches.df <- especes_fiches_lire()
  faune.df <- especes_faune_lire()
  df <- rbind(hiver.df, fiches.df, faune.df)
  df$espece <- gsub(" \\(.*", "", df$espece)
  especes.df <- as.data.frame(unique(df[, c("espece")]))
  colnames(especes.df) <- c("espece")
  especes.df$espece <- as.character(especes.df$espece)
  especes.df <- subset(especes.df, espece != "Fuligule hybride milouin x nyroca")
  especes.df <- subset(especes.df, espece != "Pigeon biset domestique")
  especes.df <- subset(especes.df, espece != "Canard hybride")
  especes.df <- as.data.frame(especes.df[order(especes.df$espece), ])
  colnames(especes.df) <- c("espece")
  especes.df$espece <- as.character(especes.df$espece)
  especes.df <<- especes.df
  return(especes.df)
}
especes_ecrire_xlsx <- function() {
  library(xlsx)
  especes.df <- especes_lire()
  dsn <- sprintf("%s\\especes.xlsx", texDir)
  write.xlsx(especes.df, dsn, row.names=FALSE)
  Log(sprintf("especes_lire_xls() %s", dsn))
}

#
# ajout des champs status
especes_statut_ecrire <- function() {
  library(xlsx)
  print(sprintf("especes_statut_ecrire()"))
  especes.df <- especes_mga_lire(TRUE)
  especes.df[] <- lapply(especes.df, as.character)
  df <- data.frame(espece=especes.df[, c("espece")])
#  print(head(df));stop("***")
#  df <- especes_stoc(df)
  df <- especes_protection(df)
  df <- especes_lrn(df)
  df <- especes_lrr(df)
  df[] <- lapply(df, as.character)
  df <- df[order(df$espece), ]
  df$LR <- ""
  df[grepl("(VU|EN|CR|NT)", df$Nicheur) | grepl("(VU|EN|CR|NT)", df$LRM) | grepl("(VU|EN|CR|NT)", df$lrr), "LR"] <- "oui"
  df <- df[, c("espece", "protection", "LR", "LRM", "Nicheur", "Hivernant", "Passage", "lrr", "rbr")]
  colnames(df) <- c("espece", "protection", "LR", "LRM", "Nicheur", "Hivernant", "Passage", "LRR", "RBR")
  dsn <- sprintf("%s\\especes_statut.xlsx", texDir)
  write.xlsx(df, dsn, row.names=FALSE)
  dsn <- sprintf("%s\\especes_statut.tex", texDir)
  Log(sprintf("especes_statut() %s", dsn))
  tex_df2table(df, dsn, num=TRUE)
  df <- subset(df, grepl("(DO)", protection) | grepl("(VU|EN|CR|NT)", Nicheur) | grepl("(VU|EN|CR|NT)", Hivernant) | grepl("(VU|EN|CR|NT)", LRM) | grepl("(VU|EN|CR|NT)", LRR))
  dsn <- sprintf("%s\\especes_statut_danger.tex", texDir)
  Log(sprintf("especes_statut() %s", dsn))
  tex_df2table(df, dsn, num=TRUE)
  return()
  df <- as.data.frame(df[, c("espece")])
  colnames(df) <- c("espece")
  df$protection <- "oui"
  return(df)
}
especes_statut_lire <- function() {
  library(xlsx)
  print(sprintf("especes_statut_lire()"))
  dsn <- sprintf("%s\\especes_statut.xlsx", texDir)
  df <- read.xlsx(dsn, 1)
  print(sprintf("especes_statut_lire() nrow : %d", nrow(df)))
  return(df)
}
#
# ajout des champs statuts
especes_statuts <- function() {
  print(sprintf("especes_statuts()"))
  df <- especes_statut_lire()
  print(head(df))
  df$statut <- ''
  for (i in 1:nrow(df) ) {
    statut <- ''
    if ( df[i, 'protection'] != "" ) {
      statut <- df[i, 'protection']
    }
    if ( df[i, 'LRM'] != "" ) {
      statut <- sprintf("%s, LRM:%s", statut, df[i, 'LRM'])
    }
    lrn <- ''
    if ( df[i, 'Nicheur'] != "" ) {
      lrn <- sprintf("%s %s(Nicheur)", lrn, df[i, 'Nicheur'])
    }
    if ( df[i, 'Hivernant'] != "" ) {
      lrn <- sprintf("%s %s(Hivernant)", lrn, df[i, 'Hivernant'])
    }
    if ( df[i, 'Nicheur'] != "" ) {
      lrn <- sprintf("%s %s(Passage)", lrn, df[i, 'Passage'])
    }
    if ( lrn != "" ) {
      statut <- sprintf("%s, LRN:%s", statut, lrn)
    }
    if ( df[i, 'LRR'] != "" ) {
      statut <- sprintf("%s, LRR:%s", statut, df[i, 'LRR'])
    }
    if ( df[i, 'RBR'] != "" ) {
      statut <- sprintf("%s %s", statut, df[i, 'RBR'])
    }
    df[i, 'statut'] <- statut
#    print(sprintf("especes_statuts() i : %d statut : %s", i, statut))
  }
  df$statut <- gsub("^, ", "", df$statut)
  df <- df[, c('espece', 'statut')]
  print(sprintf("especes_statuts() nrow : %d", nrow(df)))
  df$espece <- as.character(df$espece)
  Encoding(df$espece) <- "UTF-8"
  df$statut <- as.character(df$statut)
  Encoding(df$statut) <- "UTF-8"
  dsn <- sprintf("%s\\especes_statuts.tex", texDir)
  Log(sprintf("especes_statuts() %s", dsn))
  tex_df2table(df, dsn, num=TRUE)
  return(invisible(df))
}
#
# lecture d'un fichier avec les statuts
especes_mga_lire <- function(force=FALSE) {
  if ( exists("especes_mga.df") & force==FALSE) {
    return(especes_mga.df)
  }
  print(sprintf("especes_mga_lire()"))
  library(xlsx)
  dsn <- sprintf("%s\\especes_mga.xlsx", varDir)
  df <- read.xlsx(dsn, 1)
  cols <- colnames(df)
  df[] <- lapply(df, as.character)
  Encoding(df$espece) <- "UTF-8"
  df[is.na(df)] <- ""
  especes_mga.df <<- df
  return(invisible(df))
}
especes_mga_lire_champ <- function(champ) {
  df <- especes_mga_lire()
  df <- df[, c("espece", champ)]
  return(df)
}
#
# liste pour Matthieu
# rm(list=ls()); source("geo/scripts/vilaineaval.R");especes_mga_matthieu()
especes_mga_matthieu <- function(champ) {
  df <- especes_mga_lire()
#  View(df)
  paste(df$espece, collapse = ', ')
}
#
# création d'un fichier avec les statuts
especes_mga_xls <- function() {
  library(xlsx)
  df <- especes_mga_lire()
# nicheur/migrateur en provenance de la liste rouge régionale
  if ( 1 == 2 ) {
    df <- especes_lrr(df, "nicheur")
    df$nicheur[df$lrr != ''] <- 'oui'
    cols <- append(cols, "nicheur")
    df <- df[, cols]
    df <- especes_lrr(df, "migrateur")
    df$migrateur[df$lrr != ''] <- 'oui'
    cols <- append(cols, "migrateur")
    df <- df[, cols]
  }
  if ( 1 == 1 ) {
    df <- especes_mga_xls_status(df)
  }
  df[is.na(df)] <- ""
  df <- df[order(df$espece), ]
  dsn <- sprintf("%s\\especes_mga1.xlsx", texDir)
  write.xlsx(df, dsn, row.names=FALSE)
  Log(sprintf("especes_mga_xls() %s", dsn))
}
#
# ajout du statut de protection
especes_mga_xls_statut <- function(df) {
  statut.df <- especes_statut()
  df <- merge(x=df, y=statut.df, by.x="espece", by.y="espece", all.x=TRUE, all.y=FALSE, sort=FALSE)
#  print(head(df, 20));stop("***")
  inconnuDF <- subset(df, is.na(df$protection))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_mga_xls_status() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF[,c("espece")],20))
  }
  return(df)
}
#
# ajout des champs stoc
especes_stoc <- function(especes.df) {
  library(xlsx)
  stoc.df <- especes_stoc_espece_lire()
  stoc.df <- stoc.df[, c("nom", "specialisation")]
  stoc.df$specialisation  <- gsub("milieux ", "", stoc.df$specialisation)
  stoc.df$specialisation  <- gsub("s$", "", stoc.df$specialisation)
#  print(head(stoc.df))
  especes.df$espece_c  <- camel4(especes.df$espece)
  stoc.df$espece_c  <- camel4(stoc.df$nom)
  df <- merge(x=especes.df, y=stoc.df, by.x="espece_c", by.y="espece_c", all.x=TRUE, all.y=FALSE, sort=FALSE)
#  print(head(df, 20));stop("***")
  inconnuDF <- subset(df, is.na(df$specialisation))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_stoc() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF[,c("espece")],20))
#    print(head(inconnuDF))
#    df <- subset(stoc.df, grepl("Bergeronnette", nom))
#    print(head(df))
#    stop("especes_stoc()")
  }
  df$espece_c <- NULL
  df$nom <- NULL
  base::names(df)[base::names(df)=="specialisation"] <- "stoc"
#  print(head(df))
  return(df)
}
#
# la liste nationale
especes_lrn <- function(df) {
  cols <- colnames(df)
  df <- especes_camel_taxref(df)
#  print(head(df1));stop("***")
#  df1 <- df1[, c("espece", "CD_NOM")]
  lrn.df <- especes_lrn_taxref()
  df2 <- merge(df, lrn.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df2, is.na(df2$Famille))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_lrn() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF))
#    stop("especes_lrn()")
  }
  base::names(df2)[base::names(df2)=="espece.x"] <- "espece"
  df2[is.na(df2)] <- ""
  base::names(df2)[base::names(df2)=="De passage"] <- "Passage"
  df2 <- df2[, append(cols, c("Nicheur", "Hivernant", "Passage", "LRM"))]
#  print(head(df2, 100)); stop("***")
  return(df2)
}
#
# la liste régionale
especes_lrr <- function(df, statut="nicheur") {
  cols <- colnames(df)
  df <- especes_camel_taxref(df)
  lrr.df <- especes_lrr_taxref()
  lrr.df <- subset(lrr.df, grepl(statut, lrr.df$oiseau))
  df2 <- merge(df, lrr.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df2, is.na(df2$lrr))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_lrr() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF))
#    stop("especes_lrn()")
  }
  base::names(df2)[base::names(df2)=="espece.x"] <- "espece"
  df2[is.na(df2)] <- ""
#  print(head(df2, 100)); stop("***")
  df2 <- df2[, append(cols, c("lrr", "rbr"))]
#  print(head(df2, 100)); stop("***")
  return(df2)
}

especes_lrn_tex <- function() {
  library(data.table)
  df <- fiches_lire()
  df <- subset(df, Espece!="Pigeon biset domestique")
  especes.df <- as.data.frame(unique(df[, c("Espece")]))
  colnames(especes.df) <- c("espece")
  df1 <- especes_camel_taxref(especes.df)
  print(head(df1))
  df1 <- df1[, c("espece", "CD_NOM")]
  lrn.df <- especes_lrn_taxref()
  df2 <- merge(df1, lrn.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df2, is.na(df2$Famille))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_lrn_tex() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF))
    stop("especes_lrn()")
  }
  base::names(df2)[base::names(df2)=="espece.x"] <- "espece"
#  print(subset(df2, grepl("ron garde", espece)))
  df2 <- df2[, c("espece", "Nicheur", "Hivernant", "De passage", "LRM")]
  df2 <- subset(df2, grepl("(VU|EN|NT|CR)", Nicheur) | grepl("(VU|EN|NT|CR)", Hivernant) | grepl("(VU|EN|NT|CR)", LRM))
  df2 <- df2[order(df2$espece), ]
  df2[is.na(df2)] <- ""
  base::names(df2)[base::names(df2)=="De passage"] <- "Passage"
  print(head(df2, 100))
#  stop("***")
  dsn <- sprintf("%s\\especes_lrn.tex", texDir)
  Log(sprintf("especes_lrn() %s", dsn))
  tex_df2table(df2, dsn)
}
#
# la liste régionale
especes_lrr_tex <- function() {
  library(data.table)
  df <- fiches_lire()
  df <- subset(df, Espece!="Pigeon biset domestique")
  especes.df <- as.data.frame(unique(df[, c("Espece")]))
  colnames(especes.df) <- c("espece")
  df1 <- especes_camel_taxref(especes.df)
  df1 <- df1[, c("espece", "CD_NOM")]
  print(head(df1))
  lrr.df <- especes_lrr_taxref()
  lrr.df <- subset(lrr.df, grepl("nicheur", lrr.df$oiseau))
  df2 <- merge(df1, lrr.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
# http://stackoverflow.com/questions/9202413/how-do-you-delete-a-column-by-name-in-data-table
  df2 <- df2[-grep("espece.y", colnames(df2))]
  base::names(df2)[base::names(df2)=="espece.x"] <- "espece"
  df2 <- subset(df2, lrr != "LC")
#  df2 <- subset(df2, ordre == 'Passeriformes')
  df2 <- df2[order(df2$espece), ]
  df2 <- df2[, c("espece","lrr", "rbr")]
  print(head(df2, 100))
}
#
# la directive oiseaux
especes_protection <- function(df) {
  print(sprintf("especes_protection()"))
  cols <- colnames(df)
  df <- especes_camel_taxref(df)
  protection.df <- especes_protection_lire()
  df2 <- merge(df, protection.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df2, is.na(df2$protection))
  if ( nrow(inconnuDF) > 0 ) {
    print(sprintf("especes_protection() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF))
#    stop("especes_lrn()")
  }
  df2[is.na(df2)] <- ""
#  print(head(df2)); stop("***")
  df2 <- df2[, append(cols, c("protection"))]
#  print(head(df2, 100)); stop("***")
  return(df2)
}
especes_protection_tex <- function() {
  especes.df <- especes_lire()
  df <- especes_camel_taxref(especes.df)
  protection.df <- especes_protection_excel()
  protection.df <- subset(protection.df,  grepl("^(CC|CD|IBE|IBO)", protection.df$CD_PROTECTION))
  df <- merge(df, protection.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  df <- df[,c("CD_NOM", "espece", "CD_PROTECTION")]
#  df0 <- subset(df, grepl("Moineau domestique", df$espece))
#  print(head(df0));stop("***")
#  print(head(df, 30))
  champs <- c("CD_PROTECTION")
  df1 <- stat_champs(df, champs)
  print(head(df1, 30))
  types.df <- especes_protection_types_excel()
  df2 <- merge(df, types.df, by.x="CD_PROTECTION", by.y="CD_PROTECTION", all.x=TRUE, all.y=FALSE, sort=FALSE)
  intitule.df <- especes_protection_intitule_excel()
  df3 <- merge(df2, intitule.df, by.x="CD_PROTECTION", by.y="CD_PROTECTION", all.x=TRUE, all.y=FALSE, sort=FALSE)
#  print(head(df3, 3))
#  print(head(df3[, c("espece", "CD_PROTECTION", "libelle", "ARTICLE")], 90))
  df3 <- df3[, c("espece", "libelle", "ARTICLE")]
  df3 <- df3[order(df3$espece, df2$CD_PROTECTION), ]
  dsn <- sprintf("%s\\especes_protection.tex", texDir)
  Log(sprintf("especes_protection() %s", dsn))
  tex_df2table(df3, dsn)
}
especes_protection_lire <- function() {
  print(sprintf("especes_protection_lire_()"))
  protection.df <- especes_protection_excel()
  protection.df <- subset(protection.df,  grepl("^(CC|CD|IBE|IBO)", protection.df$CD_PROTECTION))
  protection.df <- subset(protection.df,  grepl("^(CDO1)", protection.df$CD_PROTECTION))
  types.df <- especes_protection_types_excel()
  df2 <- merge(protection.df, types.df, by.x="CD_PROTECTION", by.y="CD_PROTECTION", all.x=TRUE, all.y=FALSE, sort=FALSE)
  intitule.df <- especes_protection_intitule_excel()
  df3 <- merge(df2, intitule.df, by.x="CD_PROTECTION", by.y="CD_PROTECTION", all.x=TRUE, all.y=FALSE, sort=FALSE)
#  print(head(df3[, c("CD_NOM", "CD_PROTECTION", "libelle", "ARTICLE")], 3));stop("***")
  df3 <- df3[, c("CD_NOM", "CD_PROTECTION")]
  base::names(df3)[base::names(df3)=="CD_PROTECTION"] <- "protection"
  df3$protection <- gsub("CDO1", "DO/Annexe I", df3$protection)
#  print(head(df3)); stop("***")
  return(df3)
}
#
# les listes régionale, nationale, européenne, mondiale
especes_lrx <- function() {
  library(data.table)
  df <- fiches_lire()
  df <- subset(df, Espece!="Pigeon biset domestique")
  especes.df <- as.data.frame(unique(df[, c("Espece")]))
  colnames(especes.df) <- c("espece")
  df1 <- especes_camel_taxref(especes.df)
  df1 <- df1[, c("espece", "CD_NOM")]
  print(head(df1))
  lrx.df <- especes_lrx_lire()
  lrx.df <- subset(lrx.df, grepl("nicheur", lrx.df$TEXTCLAS))
  df2 <- merge(df1, lrx.df, by.x="CD_NOM", by.y="TAXREF_CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  df2 <- df2[, c("CD_NOM", "espece", "LRR","LRN", "LRE", "LRM")]
  df2[is.na(df2)] <- ""
  df2$statut <- sprintf("%s/%s/%s/%s", df2$LRR, df2$LRN, df2$LRE, df2$LRM)
  df2 <- subset(df2, grepl("(VU|CR|EN|NT)", df2$statut))
  df2 <- df2[, c("espece", "LRR","LRN", "LRE", "LRM")]
  df2 <- df2[order(df2$espece), ]
  print(head(df2, 60))
  dsn <- sprintf("%s\\especes_lrx.tex", texDir)
  Log(sprintf("especes_lrx() %s", dsn))
  tex_df2table(df2, dsn)
}
especes_groupes <- function(source="nicheurs") {
  dsn <- sprintf("%s/%s_combine.csv", varDir, source)
  df <- df_lire(dsn);
  df <- df[, c("NUMERO", "espece", "espece_c", "faune", "fiches")]
  colnames(df) <- c("NUMERO", "espece", "espece_c", "faune", "proto")
  mga.df <- especes_mga_lire()
#  print(head(mga.df))
  df <- merge(df, mga.df, by = c("espece"), all.x = TRUE, all.y = FALSE)
#  print(head(df));stop("***")
  texFic <- sprintf("%s\\%s_groupes.tex", texDir, source)
  TEX <- file(texFic, encoding="UTF-8")
  tex <- "% <!-- coding: utf-8 -->"
  groupes <- colnames(mga.df)
  groupes <- groupes[2:length(groupes)]
  for (i in 1:length(groupes) ) {
    groupe <- groupes[i]
    print(sprintf("groupe : %s", groupe))
    tex <- append(tex, sprintf("\\section{%s}", groupe))
    df1 <- df[df[, groupe] == "oui", c("espece") ]
    df1 <- unique(df1)
    tex <- append(tex, paste(df1, collapse=", "))
    tex <- append(tex, "\\\\*")
    dsn <- sprintf("groupes/groupe_%s", groupe)
    tex <- append(tex, sprintf("\\includegraphics[width=\\malargeurgraphique]{%s.pdf}", dsn))
    tex <- append(tex, sprintf("\\vfill"))
    tex <- append(tex, sprintf("\\pagebreak"))
    tex <- append(tex, sprintf("\\input{%s}", dsn))
    tex <- append(tex, sprintf("\\vfill"))
    tex <- append(tex, sprintf("\\clearpage"))
    sql <- sprintf("SELECT NUMERO, %s AS groupe, COUNT(*) as nb, sum(faune) as faune, sum(proto) as proto FROM df GROUP BY NUMERO, %s ORDER BY NUMERO, %s;", groupe, groupe, groupe)
    df1 <- sqldf(sql)
    especes_groupe_stat(df1, dsn)
  }
  write(tex, file = TEX, append = FALSE)
  Log(sprintf("especes_groupes() texFic: %s", texFic))
}
especes_groupe_stat <- function(df1, dsn) {
  df1 <- subset(df1, groupe != "")
  df1$groupe <- NULL
  print(head(df1));
  fic <- sprintf("%s/%s.tex", texDir, dsn)
  Log(sprintf("especes_groupe_stat() %s", fic))
  tex_df2table(df1, fic)
  fic <- sprintf("%s/%s.pdf", texDir, dsn)
  Log(sprintf("especes_groupe_stat() %s", fic))
  pdf(fic, width=4, height=4)
  par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  atlas_groupe(df1)
  dev.off()

}