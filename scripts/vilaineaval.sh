#!/bin/sh
# <!-- coding: utf-8 -->
#T Vilaine Aval
# auteur: Marc Gauthier
[ -f ../win32/scripts/misc.sh ] && . ../win32/scripts/misc.sh
[ -f ../win32/scripts/mga.sh ] && . ../win32/scripts/mga.sh
#f CONF:
CONF() {
  LOG "CONF debut"
  CFG="VILAINEAVAL"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  varDir="/d/bvi35/CouchesVilaineAval"
  LOG "CONF fin"
}

#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  E scripts/vilaineaval.sh
  for f in scripts/vilaineaval*.R ; do
    E $f
  done
  LOG "e fin"
}
#f wm:
wm() {
  LOG "wm debut"
  WM d:/web/bv d:/web.heb/bv &
  LOG "wm fin"
}
#f T:
T() {
  LOG "T debut"
  cd /d/tmp
  _ENV_im
  convert carte.pdf carte.png
  LOG "T fin"
}
#f JOUR:
JOUR() {
  LOG "JOUR debut"
  perl scripts/biolovision.pl BH rvaExport
  LOG "JOUR fin"
}
#F FB_pl: recup des donnees Faune Bretagne
FB_pl() {
  LOG "FB_pl debut"
#  perl scripts/biolovision.pl BH rvaExportXls
  perl scripts/biolovision.pl BH rvaEtangsXls
#  wc -l ${CFG}/donnees.txt
  LOG "FB_pl fin"
}
#F FB_R: traitement des donnees Faune Bretagne
FB_R() {
  LOG "FB_R debut"
  _ENV_R
#  R --help
  ( cd ..; R --vanilla   -e "source('geo/scripts/miscInstall.R');" )
#  ( cd ..; R --vanilla   -e "source('geo/scripts/vilaineaval.R');" )
#  ( cd ${CFG}/images; explorer . &)
  LOG "FB_R fin"
}
#f FB_TEX: mise en page
FB_TEX() {
  LOG "FB_TEX debut"
  _ENV_tex
  file=faune
  (
    cd ${CFG}
    rm $file*aux
    rm $file*bbl
    pdflatex ${file}.tex
    pdflatex ${file}.tex
  )
  LOG "FB_TEX fin"
}
#F PARCOURS_R:
PARCOURS_R() {
  LOG "PARCOURS_R debut"
  _ENV_R
  ( cd ..; R --vanilla   -e "source('geo/scripts/vilaineaval.R'); parcours()" )
  ( cd ${CFG}/images; explorer . &)
  LOG "PARCOURS_R fin"
}
#f SERENA_up:
SERENA_up() {
  LOG "SERENA_up debut"
  site=bv_ovh; . ../win32/WEB/$site.site
  curl --ftp-create-dirs -T VILAINEAVAL/serena_proto.geojson -u "${ftp_user}:${ftp_password}" ftp://${ftp_host}/www/bvo35rva/serena_proto.geojson
  LOG "SERENA_up fin"
}
#f ETANGS_parcours:
ETANGS_parcours() {
  LOG "ETANGS_parcours debut"
  _ENV_gdal
  set -x
  ogr2ogr -f "GeoJSON" -lco COORDINATE_PRECISION=5 ${varDir}/etangs_parcours.json ${varDir}/etangs_parcours.gpx tracks
  LOG "ETANGS_parcours fin"
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=vilaineaval; Remote=frama
  export Local
  export Depot
  export Remote
  _git_lst
  bash ../win32/scripts/git.sh INIT "$*"
#  bash ../win32/scripts/git.sh PUSH
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/git.lst
scripts/vilaineaval.sh
EOF
  ls -1 scripts/vilaineaval*.R >> /tmp/git.lst
  ls -1 VILAINEAVAL/*.tex >> /tmp/git.lst
#  ls -1 VILAINEAVAL/images/*.pdf >> /tmp/git.lst
  cat  <<'EOF' > /tmp/README.md
# vilaineaval : Bretagne Vivante et Rennes Vilaine Aval

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Serena et Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier VILAINEAVAL.

Les fichiers produits par R sont dans VILAINEAVAL/images

EOF
}
#F MARES_fb:
MARES_fb() {
  LOG "MARES_fb debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R');mares_jour();" )
  LOG "MARES_fb fin"
}
#F MARES_va:
MARES_va() {
  LOG "MARES_va debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/vilaineaval.R'); mares_jour();" )
  LOG "MARES_va fin"
}
#f MARES_tex:
MARES_tex() {
  LOG "MARES_tex debut"
  file=mares_fb
  _ENV_tex
  (
    cd ${CFG}
    rm $file*aux
    rm $file*bbl
    pdflatex ${file}.tex
    pdflatex ${file}.tex
    PDF ${file}.pdf
  )
  LOG "MARES_tex fin"
}
#F RVA_fb:
RVA_fb() {
  LOG "RVA_fb debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/fb.R');rva_jour();" )
  LOG "RVA_fb fin"
}
#F RVA_va:
RVA_va() {
  LOG "RVA_va debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/vilaineaval.R'); nicheurs_bvo35_csv();" )
  ( cd ..; R --vanilla -e "source('geo/scripts/vilaineaval.R'); nicheurs_fb2liste();" )
  LOG "RVA_va fin"
}
#F RVA_fi:
RVA_fi() {
  LOG "RVA_va debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/vilaineaval.R'); fiches_bvo35_csv();" )
  LOG "RVA_fi fin"
}
#F RVA_canal:
RVA_canal() {
  LOG "RVA_va debut"
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/vilaineaval.R'); tex_canal();" )
  LOG "RVA_canal fin"
}
#f RVA_tex:
RVA_tex() {
  LOG "RVA_tex debut"
  file=nicheurs_fb
  _ENV_tex
  (
    cd ${CFG}
    rm $file*aux
    rm $file*bbl
    pdflatex ${file}.tex
    pdflatex ${file}.tex
    PDF ${file}.pdf
  )
  LOG "RVA_tex fin"
}
#f RVA_up:
RVA_up() {
  LOG "RVA_up debut"
  . ../win32/WEB/ao35.site
  while read src ; do
    f=`basename $src`
    curl --ftp-create-dirs -T $src \
      -u "${ftp_user}:${ftp_password}" ftp://${ftp_host}/mares35/$f
  done <<EOF
VILAINEAVAL/nicheurs_fb.pdf
EOF
  LOG "RVA_up fin"
}
#f BVO35_tex:
BVO35_tex() {
  LOG "BVO35_tex debut"
  file=bvo35
  _ENV_tex
  (
    cd ${CFG}
    rm $file*aux
    rm $file*bbl
    pdflatex ${file}.tex
    pdflatex ${file}.tex
    PDF ${file}.pdf
  )
  LOG "BVO35_tex fin"
}
RVA_tex() {
  _ENV_pandoc
  texDir=${CFG}/canal
  cd $texDir
  mv -v /c/Users/Marc/Downloads/canal*.txt .
  for md in canal_*.txt; do
    tex=`basename "$md" .txt`
    pandoc -f markdown $md -o $tex.tex
#    cat $tex.tex
  done
}
#F RVA_jour:
RVA_jour() {
  LOG "RVA_jour debut"
  RVA_fb
  RVA_va
  RVA_tex
#  RVA_up
  LOG "RVA_jour fin"
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done