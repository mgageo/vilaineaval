# <!-- coding: utf-8 -->
#
# la partie oiseaux en hiver
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# deux sources de données :
# serena pour les parcours
# faune-bretagne pour les opportunistes
# que les données en hiver
#
hiver <- function() {
  carp()
#  hiver_mailles_especes_ecrire()
#  stop("***")
#  hiver_combine_ecrire(); # génération du fichier des données
  hiver_combine(); # les stat
#  hiver_combine_atlas()
}
# extraction des données faune-bretagne
# source("geo/scripts/vilaineaval.R"); df <- hiver_faune_lire()
hiver_faune_lire <- function() {
  dsn <- "rva_donnees.xlsx"
#  dsn <- "etienne_hiver.xlsx"
  carp('dsn: %s', dsn)
  spdf <- faune_lire_xls(dsn)
#  spdf <- faune_lire_atlas(spdf)
  df <- spdf@data
#  print(head(spdf@data))
  date_from <- as.Date("01/12/2016", "%d/%m/%Y")
  date_to <- as.Date("20/01/2017", "%d/%m/%Y")
  date_from <- as.Date("01/12/2018", "%d/%m/%Y")
  date_to <- as.Date("20/01/2019", "%d/%m/%Y")
# sur plusieurs années
#  spdf <- spdf[spdf@data$quantieme >= quantieme_from | spdf@data$quantieme <= quantieme_to,]
  df <- df[df$d >= date_from,]
  df <- df[df$d <= date_to,]
  carp("hiver_faune_lire() nrow: %d", nrow(df))
  df %>%
    group_by(NAME) %>%
    summarize(nb=n()) %>%
    View(.)
#  stop("***")
  return(invisible(df))
}
# source("geo/scripts/vilaineaval.R"); spdf <- hiver_faune_lire_etienne()
hiver_faune_lire_etienne <- function() {
  dsn <- "etienne_hiver.xlsx"
  dsn <- "rva_donnees.xlsx"
  carp('dsn: %s', dsn)
  dsn <- sprintf("%s/%s", fbDir, dsn)
  carp("dsn: %s", dsn)
  df <- read_excel(dsn)
  carp("nrow: %s", nrow(df))
# la première ligne en moins
  df <- df[-1,]
  df <- df %>%
    filter(grepl('^BVO35RVA', COMMENT)) %>%
    filter(NAME == 'Rogeau')
  df %>%
    group_by(NAME) %>%
    summarize(nb=n()) %>%
    View(.)
#  stop("***")
# transformation en spatial
  df [,"COORD_LAT"] <- sapply(df[,"COORD_LAT"], as.character)
  df [,"COORD_LAT"] <- sapply(df[,"COORD_LAT"], as.numeric)
  df [,"COORD_LON"] <- sapply(df[,"COORD_LON"], as.character)
  df [,"COORD_LON"] <- sapply(df[,"COORD_LON"], as.numeric)
  bug.df <- subset(df, COORD_LAT < 47.6)
  if ( nrow(bug.df) > 0 ) {
    carp('***COORD_LAT')
    View(bug.df)
    stop("***")
  }
  coordinates(df) = ~ COORD_LON + COORD_LAT
#  print(sapply(df, class))
  spdf <- SpatialPointsDataFrame(df,data.frame(df[,]))
  proj4string(spdf) <- CRS("+init=epsg:4326")
#  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  carp("nrow: %d", nrow(spdf@data))
  return(invisible(spdf))
}
hiver_mailles_especes_ecrire <- function() {
  print(sprintf("hiver_mailles_especes_ecrire()"))
  dsn <- sprintf("%s/hiver_mailles_especes.csv", varDir)
  df <- hiver_faune_lire()
  faune_mailles_especes_ecrire(df, dsn)
  serena_mailles_especes()
}
hiver_mailles_especes_lire <- function() {
  dsn <- sprintf("%s/hiver_mailles_especes.csv", varDir)
  faune.df <- faune_mailles_especes_lire(dsn)
  serena.df <- serena_mailles_especes_lire()
}
#
# lecture de toutes les sources
hiver_combine_ecrire <- function() {
  library(xlsx)
  Log(sprintf("hiver_combine_ecrire()"))
  faune.df <- atlas_mailles_especes_lire("hiver")
  serena.df <- atlas_mailles_especes_lire("serena_proto")
  faune.df$espece[faune.df$espece == "Pigeon biset domestique"] <- "Pigeon biset"
  faune.df$espece_c[faune.df$espece_c == "PigeonBisetDomestique"] <- "PigeonBiset"
  faune.df$espece[faune.df$espece_c == "GallinulePouleDEau"] <- "Gallinule poule-d'eau"
#  print(head(faune.df[grep("Galli", faune.df$espece),]))
  serena.df$espece[serena.df$espece_c == "GallinulePouleDEau"] <- "Gallinule poule-d'eau"
#  print(head(serena.df[grep("Galli", serena.df$espece_c),]))
#  stop("***")
  df <- merge(faune.df, serena.df, by = c("NUMERO", "espece", "espece_c"), all.x = TRUE, all.y = TRUE)
  df$nb.x[is.na(df$nb.x)] <- 0
  df$nb.y[is.na(df$nb.y)] <- 0
  colnames(df)[colnames(df) == 'nb.x'] <- 'faune'
  colnames(df)[colnames(df) == 'nb.y'] <- 'proto'
  dsn <- sprintf("%s/hiver_combine.csv", varDir)
  df_ecrirex(df, dsn);
  Log(sprintf("hiver_combine_ecrire() dsn: %s", dsn))
#  return(df)
}
hiver_combine <- function() {
  carp()
  dsn <- sprintf("%s/hiver_combine.csv", varDir)
  df <- df_lire(dsn);
  atlas_combine_carte(df, "hiver")
}
hiver_combine_atlas <- function() {
  carp()
  dsn <- sprintf("%s/hiver_combine.csv", varDir)
  df <- df_lire(dsn);
  atlas_especes_combine(df, "hiver")
}
hiver__top <- function() {
  library(dplyr)
  df <- serena_mailles_especes_lire()
  df <- df %>% group_by(TAXO_VERNACUL) %>%  summarise(nb = n())
  top.df <- df %>% filter(rank(desc(nb))<=25)
}
