# <!-- coding: utf-8 -->
#
# la partie occupation du sol
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# http://journals.openedition.org/cybergeo/27067?lang=en
# https://github.com/mapbox/robosat reconnaissance automatique sur des images, apprentissage OpenStreetMap
#
# la génération des fichiers tex
ocs_tex <- function() {
  les_tex <- read.table(text="fonction|mode|args
fonds_oso_carte|carte|
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  tex_les(les_tex)
}
#
# les données du fonds oso pour les mailles
# http://osr-cesbio.ups-tlse.fr/~oso/
ocs_oso_mailles <- function() {
  dsn <- sprintf("%s/nord_lambert_500.json", varDir)
  spdf <- ogr_lire(dsn);
  df <- fonds_oso_nomenclature_stat(spdf, "NUMERO")
  dsn <- sprintf("%s\\ocs_oso_nomenclature.csv", texDir)
  write.table(df, file = dsn, sep = "\t", row.names=FALSE, col.names = TRUE,qmethod = "double", quote = FALSE, fileEncoding="utf8")
  dsn <- sprintf("%s\\ocs_oso_nomenclature.tex", texDir)
  tex_df2table(df, dsn)
  df <- fonds_oso_stat(spdf, "NUMERO")
  dsn <- sprintf("%s\\ocs_oso_mailles.csv", texDir)
  write.table(df, file = dsn, sep = "\t", row.names=FALSE, col.names = TRUE,qmethod = "double", quote = FALSE, fileEncoding="utf8")
  dsn <- sprintf("%s\\ocs_oso_mailles.tex", texDir)
  tex_df2table(df, dsn)
}
ocs_oso_points <- function() {
  dsn <- sprintf("%s/points.geojson", varDir)
  points.spdf <<- ogr_lire(dsn);
  spdf <- gBuffer(points.spdf, width=100, byid=TRUE)
  df <- fonds_oso_stat(spdf, "numero")
  dsn <- sprintf("%s\\ocs_oso_points.csv", texDir)
  write.table(df, file = dsn, sep = "\t", row.names=FALSE, col.names = TRUE,qmethod = "double", quote = FALSE, fileEncoding="utf8")
  dsn <- sprintf("%s\\ocs_oso_points.tex", texDir)
  tex_df2table(df, dsn)
}
# https://eric.univ-lyon2.fr/~ricco/cours/didacticiels/R/cah_kmeans_avec_r.pdf
ocs_cah <- function() {
  library(stats)
  dsn <- sprintf("%s\\ocs_oso_mailles.csv", texDir)
  df <- read.table(dsn,row.names=1, header=TRUE, sep="\t", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", encoding="UTF-8")
  print(head(df))
  pairs(df)
  cr <- scale(df,center=T,scale=T)
  d <- dist(cr)
  cah.ward <- hclust(d, method="ward.D2")
  plot(cah.ward)
  rect.hclust(cah.ward, k=4)
  groupes.cah <- cutree(cah.ward, k=4)
  print(sort(groupes.cah))
}

# https://gis.stackexchange.com/questions/140504/extracting-intersection-areas-in-r
ocs_mailles_surface <- function() {
  library(sp)
  library(raster)
  library(rgdal)
  library(rgeos)
  library(data.table)
  sol.spdf <- fonds_sol_lire()
  mailles.spdf <- fonds_grille_lire()
  spdf <- intersect(sol.spdf, mailles.spdf)
  spdf@data$surface <- as.integer(rgeos::gArea(spdf, byid=TRUE))
  print(head(spdf))
  df <- spdf@data
  setnames(df, "NUMERO", "numero")
  ocs_surface(df)
}
ocs_points_surface <- function() {
  library(sp)
  library(raster)
  library(rgdal)
  library(rgeos)
  sol.spdf <- fonds_sol_lire()
#  plot(sol.spdf, col=sol.spdf@data$couleur, lwd = .1)
  dsn <- sprintf("%s/points.geojson", varDir)
  points.spdf <<- ogr_lire(dsn);
  sp <- gBuffer(points.spdf, width=100, byid=TRUE)
  spdf <- intersect(sol.spdf, sp)
  spdf@data$surface <- as.integer(rgeos::gArea(spdf, byid=TRUE))
  print(head(spdf))
  df <- spdf@data
}
# https://www.r-bloggers.com/aggregate-a-powerful-tool-for-data-frame-in-r/
# https://cran.r-project.org/doc/contrib/Genolini-LireCompterTesterR.pdf
# http://brooksandrew.github.io/simpleblog/articles/advanced-data-table/
# http://stackoverflow.com/questions/16513827/r-summarizing-multiple-columns-with-data-table
# http://blog.yhat.com/posts/fast-summary-statistics-with-data-dot-table.html
ocs_surface <- function(df) {
  library(data.table)
  dt <- setDT(df)
  dt1 <- dt[, list(surface=sum(surface)), by=list(numero)]
  dt1 <- dt1[order(dt1[,1]),]
  print(head(dt1,20))
  dt1 <- dt[, list(surface=sum(surface)), by=list(numero, nature)]
  dt1 <- dt1[order(dt1[,1], dt1[,2]),]
  print(head(dt1,20))
}