# <!-- coding: utf-8 -->
#
# la partie mares
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
mares_conf <- function() {
  webDir <<- sprintf("%s/bvi35/CouchesVilaineAval", Drive);
  fbDir <<- sprintf("%s/bvi35/CouchesFB", Drive);
  serenaDir <<- sprintf("%s/bvi35/CouchesSerena", Drive);
  webDir <<- sprintf("%s/web.heb/bv/mares35", Drive);
}
mares_jour <- function() {
  if ( exists("faune.spdf") ) {
    rm("faune.spdf", pos = ".GlobalEnv")
  }
  mares_conf()
  mares_get()
#  cartes_mares_tex()
#  tex_mares()
  mares_fb2mare()
}
tex_mares <- function() {
  les_tex <- read.table(text="fonction|mode|args
mares_carte|carte|
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  tex_les(les_tex)
}
# lecture d'un fichier ogr
mares_get <- function(d) {
  library(RCurl)
  dsn <- 'http://bretagne-vivante-dev.org/mares35/mares35.php?action=get_mares'
  dsn <- 'http://ao35.free.fr/mares35/mares35.php?action=get_mares'
#  dsn <- 'http://bv/mares35/mares35.php?action=get_mares'
  Log(sprintf("mares_get() dsn: %s", dsn))
  txt <- getURL(dsn)
  dsn <- sprintf("%s/mares.geojson", varDir)
  write(txt,file=dsn);
  Log(sprintf("mares_get() dsn: %s", dsn))
}
# lecture d'un fichier ogr
#
mares_lire <- function(geomType="wkbPoint") {
  require(rgdal)
  require(rgeos)
  dsn <- sprintf("%s/mares.geojson", varDir)
  Log(sprintf("mares_lire() dsn:%s", dsn))
  layer <- ogrListLayers(dsn)
#  Log(sprintf("mares_lire() %s %s", layer, dsn))
  spdf <- readOGR(dsn, layer=layer, stringsAsFactors=FALSE, use_iconv=TRUE, encoding="UTF-8", require_geomType=geomType)
  spdf <- spTransform(spdf, CRS("+init=epsg:2154"))
  return(spdf)
}
mares_carte <- function() {
  Log(sprintf("mares_carte()"))
  spdf <- mares_lire()
  fonds_raster()
  fonds_grille()
  plot(spdf, add=TRUE, lwd=4, col="orange")
}
mares_carte_triton <- function() {
  Log(sprintf("mares_carte_triton()"))
  require(raster)
  spdf <- mares_spdf2polygon()
  spdf1 <- spdf[grepl("^(1|10|14|16|3|49|50|56|64|72|73)$", spdf@data$numero), ]
  sp500 <- gBuffer(spdf1, width=500, byid=FALSE)
  sp1000 <- gBuffer(spdf1, width=1000, byid=FALSE)
  print(sprintf("mares_carte_triton() nrow : %s", nrow(spdf1@data)))
  rasterFic <-  sprintf("%s/%s_GP_PVA_15_2154.tif", varDir, secteur);
  Log(sprintf("mares_carte_triton() rasterFic: %s", rasterFic))
  raster1 <- brick(rasterFic)
  raster1 <- fonds_raster_extrait_spdf(raster1, sp1000)
  plotRGB(raster1,axes=FALSE,add=FALSE)
  text(coordinates(spdf1), labels=spdf1@data$numero, font = 2, cex= 1.5, col = "orange")
#  plot(spdf1, add=TRUE, lwd=1)
  plot(sp500, add=TRUE, lwd=4, border="orange")
  plot(sp1000, add=TRUE, lwd=2)
}
mares_stat_champ <- function(champ= 'prenom') {
  Log(sprintf("mares_stat_champ() champ:%s", champ))
  spdf <- mares_lire()
  df <- spdf@data
  p <- stat_champ(df, champ)
  return(p)
}
mares_liste <- function() {
  library(gtools)
  mares.spdf <- mares_lire()
  mares <- mixedsort(mares.spdf$numero)
  for ( i in 1:length(mares) ) {
    mare <- mares[i]
    print(mare);
  }
}
#
# transformation en polygon
mares_spdf2polygon <- function() {
  Log(sprintf("mares_spdf2polygon()"))
  library(raster)
  mares.point.spdf <- mares_lire("wkbPoint")
  mares.line.spdf  <- mares_lire("wkbLineString")
  mares.polygon.spdf  <- mares_lire("wkbPolygon")
  sp <- gBuffer( mares.point.spdf, width=1, byid=TRUE )
  point.spdf <- SpatialPolygonsDataFrame(sp, data=mares.point.spdf@data )
  sp <- gBuffer( mares.line.spdf, width=1, byid=TRUE )
  line.spdf <- SpatialPolygonsDataFrame(sp, data=mares.line.spdf@data )
  mares.spdf <- raster::union(point.spdf, line.spdf)
  mares.spdf <- raster::union(mares.spdf, mares.polygon.spdf)
  print(sprintf("mares_spdf2polygon() nrow : %d", nrow(mares.spdf)))
  return(mares.spdf)
}
#
# affectation d'une mare à une donnée
mares_spdf2mare <- function(spdf) {
  Log(sprintf("mares_spdf2mare()"))
  mares.point.spdf <- mares_lire("wkbPoint")
  mares.line.spdf  <- mares_lire("wkbLineString")
  mares.polygon.spdf  <- mares_lire("wkbPolygon")
#  Log(sprintf("mares_spdf2mare() nb spdf:%d", nrow(spdf)))
  for ( i in 1:nrow(spdf) ) {
#    spdf$numero[i] <- mares.spdf$numero[which.min(gDistance(spdf[i,], mares.spdf, byid=TRUE))]
    g <- gDistance(spdf[i,], mares.point.spdf, byid=TRUE)
    j <- which.min(g)
    d <- g[j]
    spdf$point_n[i] <- mares.point.spdf$numero[j]
    spdf$point_d[i] <- d
    spdf$numero[i] <- mares.point.spdf$numero[j]
    spdf$distance[i] <- d
    spdf$type[i] <- 'mare'
# pour les lignes
    g <- gDistance(spdf[i,], mares.line.spdf, byid=TRUE)
    j <- which.min(g)
    d <- g[j]
    spdf$line_n[i] <- mares.line.spdf$numero[j]
    spdf$line_d[i] <- d
    if ( d < spdf$distance[i] ) {
      spdf$numero[i] <- mares.line.spdf$numero[j]
      spdf$distance[i] <- d
      spdf$type[i] <- 'transect'
    }
# pour les surfaces
    g <- gDistance(spdf[i,], mares.polygon.spdf, byid=TRUE)
    j <- which.min(g)
    d <- g[j]
    spdf$polygon_n[i] <- mares.polygon.spdf$numero[j]
    spdf$polygon_d[i] <- d
    if ( d < spdf$distance[i] ) {
      spdf$numero[i] <- mares.polygon.spdf$numero[j]
      spdf$distance[i] <- d
      spdf$type[i] <- 'mare'
    }
  }
  return(spdf)
}
#
# https://cran.r-project.org/web/packages/spdep/vignettes/nb.pdf
# http://stackoverflow.com/questions/22121742/calculate-the-distance-between-two-points-of-two-datasets-nearest-neighbor
# http://stackoverflow.com/questions/21977720/r-finding-closest-neighboring-point-and-number-of-neighbors-within-a-given-rad
# https://www.datacamp.com/community/tutorials/15-easy-solutions-data-frame-problems-r#gs.lCoU6PI
mares_fb2journee <- function() {
  Log(sprintf("mares_fb2journee()"))
  fb.spdf <- fb_lire('mares');
  fb.spdf <- mares_spdf2mare(fb.spdf)
#  Log(sprintf("mares_fb2journee() nb fb.spdf:%d", nrow(fb.spdf)))
  df <- fb.spdf@data
  df$distance <- round(df$distance)
  df <- df[order(df$DATE), ]
#  date_from <- as.Date("01/01/2017", "%d/%m/%Y")
#  df <- df[df$d >= date_from,]
  Log(sprintf("mares_fb2journee() nrow: %d", nrow(df)))
#
# les journées
  champs <- c("DATE")
  df1 <- stat_champs(df, champs)
#  print(head(df1, 60))
  texFic <- sprintf("%s\\mares_fb2journee.tex", texDir)
  TEX <- file(texFic, encoding="UTF-8")
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(df1) ) {
    D <- df1[i, "DATE"]
    df2 <- df[df$DATE == D,]
    champs <- c("observateur")
    df3 <- stat_champs(df2, champs)
    d <- as.Date(as.numeric(as.character(D)), origin="1899-12-30")
    jour <- strftime(d, format="%a %d/%m/%Y")
    for ( j in 1:nrow(df3) ) {
      obs <- df3[j, "observateur"]
      print(sprintf("%s;%s", strftime(d, format="%d/%m/%Y"), obs))
      tex <- append(tex, sprintf("\\subsection*{%s %s}", jour, obs))
      df4 <- subset(df, observateur == obs)
      df4 <- subset(df4, DATE == D)
#      print(sprintf("df4:%d", nrow(df4)))
      for ( k in 1:nrow(df4) ) {
        tex <- append(tex, sprintf("\\subsubsection*{\\href{http://www.faune-bretagne.org/index.php?m_id=54&id=%s}{fb }%s(%s)}", df4[k, "ID_SIGHTING"], df4[k, "NAME_SPECIES"], df4[k, "TOTAL_COUNT"]))
        mortalite <- df4[k, "HAS_DEATH_INFO"]
        commune <- df4[k, "MUNICIPALITY"]
        lieudit <- df4[k, "PLACE"]
        type <- df4[k, "type"]
        lieu <- df4[k, "numero"]
        distance <- df4[k, "distance"]
        tex <- append(tex, sprintf("%s;mort:%s\\\\", lieudit, mortalite))
        tex <- append(tex, sprintf("%s:%s(%s m)\\\\", type, lieu, distance))
#        tex <- append(tex,"\\begin{lstlisting}")
        tex <- append(tex, sprintf("%s", df4[k, "COMMENT"]))
#        tex <- append(tex,"\\end{lstlisting}")
      }
    }
  }
  write(tex, file = TEX, append = FALSE)
  Log(sprintf("mares_fb2journee() texFic: %s", texFic))
}
#
# liste des observations par mare
mares_fb2mare <- function() {
  Log(sprintf("mares_fb2mare()"))
  fb.spdf <- fb_lire('mares');
  fb.spdf <- mares_spdf2mare(fb.spdf)
#  Log(sprintf("mares_fb2mare() nb fb.spdf:%d", nrow(fb.spdf)))
  df <- fb.spdf@data
#  df <- subset(df, grepl("Triton cr", df$NAME_SPECIES))
  df$distance <- round(df$distance)
  df <- df[order(df$numero), ]
#  date_from <- as.Date("01/01/2017", "%d/%m/%Y")
#  df <- df[df$d >= date_from,]
  Log(sprintf("mares_fb2mare() nrow: %d", nrow(df)))
#  print(head(df)); stop("***")
#
# les mares
  champs <- c("numero")
  df1 <- stat_champs(df, champs)
#  print(head(df1, 60))
  texFic <- sprintf("%s\\mares_fb2mare.tex", texDir)
  TEX <- file(texFic, encoding="UTF-8")
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(df1) ) {
    n <- df1[i, "numero"]
    df2 <- df[df$numero == n,]
    tex <- append(tex, sprintf("\\subsection*{mare %s}", n))
#    print(head(df2));stop("***")
    for ( j in 1:nrow(df2) ) {
      D <- df2[j, "DATE"]
      d <- as.Date(as.numeric(as.character(D)), origin="1899-12-30")
      jour <- strftime(d, format="%a %d/%m/%Y")
      obs <- df2[j, "observateur"]
#      tex <- append(tex, sprintf("\\subsection*{%s %s}", jour, obs))
      tex <- append(tex, sprintf("\\subsubsection*{\\href{http://www.faune-bretagne.org/index.php?m_id=54&id=%s}{fb }%s(%s)}", df2[j, "ID_SIGHTING"], df2[j, "NAME_SPECIES"], df2[j, "TOTAL_COUNT"]))
      tex <- append(tex, sprintf("%s;%s\\\\", strftime(d, format="%d/%m/%Y"), obs))
      mortalite <- df2[j, "HAS_DEATH_INFO"]
      commune <- df2[j, "MUNICIPALITY"]
      lieudit <- df2[j, "PLACE"]
      type <- df2[j, "type"]
      lieu <- df2[j, "numero"]
      distance <- df2[j, "distance"]
      tex <- append(tex, sprintf("%s;mort:%s\\\\", lieudit, mortalite))
      tex <- append(tex, sprintf("%s:%s(%s m)\\\\", type, lieu, distance))
#        tex <- append(tex,"\\begin{lstlisting}")
      tex <- append(tex, sprintf("%s", df2[j, "COMMENT"]))
#        tex <- append(tex,"\\end{lstlisting}")
    }
  }
  write(tex, file = TEX, append = FALSE)
  Log(sprintf("mares_fb2mare() texFic: %s", texFic))
}
#
# http://stackoverflow.com/questions/20396582/order-a-mixed-vector-numbers-with-letters
mares_serena2mare <- function() {
  Log(sprintf("mares_serena2mare()"))
  library(gtools)
  spdf <- serena_mares_zone();
  spdf <- mares_spdf2mare(spdf)
  spdf$TAXO_VERNACUL <- iconv(spdf$TAXO_VERNACUL, "UTF-8")
  spdf$OBSE_PLACE <- iconv(spdf$OBSE_PLACE, "UTF-8")
  spdf$OBSE_CONTACT_CHOI <- iconv(spdf$OBSE_CONTACT_CHOI, "UTF-8")
  Log(sprintf("mares_serena2mare() nb spdf:%d", nrow(spdf)))
  df <- spdf@data
  df$distance <- round(df$distance)
  df <- df[order(df$OBSE_DATE), ]
  Log(sprintf("mares_serena2mare() nrow: %d", nrow(df)))
#
# les mares
  champs <- c("numero", "type")
  df1 <- stat_champs(df, champs)
  v1 <- as.numeric(sub("\\D+", "", df1$numero))
  v2 <- sub("\\d+", "", df1$numero)
  df1 <- df1[order(v2, v1),]
#  print(head(df1[,c("type", "numero")], 20))
  texFic <- sprintf("%s\\mares_serena2mare.tex", texDir)
  TEX <- file(texFic, encoding="UTF-8")
  tex <- "% <!-- coding: utf-8 -->"
  for ( i in 1:nrow(df1) ) {
    numero <- df1[i, "numero"]
    type <- df1[i, "type"]
    df2 <- df[df$numero == numero,]
#    print(head(df2))
    tex <- append(tex, sprintf("\\subsection*{%s %s}", type, numero))
    for ( j in 1:nrow(df2) ) {
      tex <- append(tex, sprintf("\\subsubsection*{%s %s(%s)}", df2[j, "OBSE_DATE"], df2[j, "TAXO_VERNACUL"], df2[j, "OBSE_NOMBRE"]))
      type <- df1[j, "type"]
      lieu <- df1[j, "numero"]
      distance <- df1[j, "distance"]
      tex <- append(tex, sprintf("%s;choix:%s\\\\", df2[j, "OBSE_PLACE"], df2[j, "OBSE_CONTACT_CHOI"]))
      tex <- append(tex, sprintf("%s:%s(%s m)\\\\", type, lieu, distance))
    }
  }
  write(tex, file = TEX, append = FALSE)
  Log(sprintf("mares_serena2mare() texFic: %s", texFic))
}
